/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import java.util.ArrayList;

/**
 *
 * @author jlovell4
 */
public class LandlordList {
    private ArrayList<Landlord> list;
    
    public LandlordList() {
        list = new ArrayList<Landlord>();
    }
    
    public void addLandlord(Landlord newLandlord) {
        list.add(newLandlord);
    }
    
    public int size() {
        return list.size();
    }
    
    public Landlord getLandlordAt(int index) {
        if (index < 0 || index >= list.size())
            return null;
        else
            return list.get(index);
    }

    public ArrayList<Landlord> getList() {
        return list;
    }
    
    
}
