/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Classes;

import java.util.ArrayList;

/**
 *
 * @author Jamie
 */
public class PropertyList {
    private ArrayList<Property> list;
    
    public PropertyList() {
        list = new ArrayList<Property>();
    }
    
    public void addProperty(Property newProperty) {
        list.add(newProperty);
    }
    
    public int size() {
        return list.size();
    }
    
    public Property getPropertyAt(int index) {
        if (index < 0 || index >= list.size())
            return null;
        else
            return list.get(index);
    }

    public ArrayList<Property> getList() {
        return list;
    }

    public void setList(ArrayList<Property> list) {
        this.list = list;
    }
    
    
}
