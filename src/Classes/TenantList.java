/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import java.util.ArrayList;

/**
 *
 * @author jlovell4
 */
public class TenantList {
    private ArrayList<Tenant> list;
    
    public TenantList() {
        list = new ArrayList<Tenant>();
    }
    
    public void addTenant(Tenant newTenant) {
        list.add(newTenant);
    }
    
    public int size() {
        return list.size();
    }
    
    public Tenant getTenantAt(int index) {
        if (index < 0 || index >= list.size())
            return null;
        else
            return list.get(index);
    }

    public ArrayList<Tenant> getList() {
        return list;
    }
    
    
}
