/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import java.util.ArrayList;

/**
 *
 * @author jlovell4
 */
public class EnquiryList {
    private ArrayList<Enquiry> list;
    
    public EnquiryList() {
        list = new ArrayList<Enquiry>();
    }
    
    public void addEnquiry(Enquiry newEnquiry) {
        list.add(newEnquiry);
    }
    
    public int size() {
        return list.size();
    }
    
    public Enquiry getEnquiryAt(int index) {
        if (index < 0 || index >= list.size())
            return null;
        else
            return list.get(index);
    }

    public ArrayList<Enquiry> getList() {
        return list;
    }
    
    
}
