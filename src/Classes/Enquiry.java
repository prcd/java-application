/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Classes;

import java.util.ArrayList;

/**
 *
 * @author Jamie
 */
public class Enquiry {
    protected int ID;
    protected String guestEmail;
    protected String guestPhone;
    protected int propertyID;

    public Enquiry(int ID, String guestEmail, String guestPhone, int propertyID) {
        this.ID = ID;
        this.guestEmail = guestEmail;
        this.guestPhone = guestPhone;
        this.propertyID = propertyID;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getGuestEmail() {
        return guestEmail;
    }

    public void setGuestEmail(String guestEmail) {
        this.guestEmail = guestEmail;
    }

    public String getGuestPhone() {
        return guestPhone;
    }

    public void setGuestPhone(String guestPhone) {
        this.guestPhone = guestPhone;
    }

    public int getPropertyID() {
        return propertyID;
    }

    public void setPropertyID(int propertyID) {
        this.propertyID = propertyID;
    }

    
    
}
