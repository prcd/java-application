/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import java.util.ArrayList;

/**
 *
 * @author jlovell4
 */
public class RepairList {
    private ArrayList<Repair> list;
    
    public RepairList() {
        list = new ArrayList<Repair>();
    }
    
    public void addRepair(Repair newRepair) {
        list.add(newRepair);
    }
    
    public int size() {
        return list.size();
    }
    
    public Repair getRepairAt(int index) {
        if (index < 0 || index >= list.size())
            return null;
        else
            return list.get(index);
    }

    public ArrayList<Repair> getList() {
        return list;
    }
    
    
}
