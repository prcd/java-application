/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package propertyAccess;

import Classes.Enquiry;
import Classes.EnquiryList;
import Classes.Landlord;
import Classes.LandlordList;
import Classes.Property;
import Classes.PropertyList;
import Classes.Repair;
import Classes.RepairList;
import Classes.Tenant;
import Classes.TenantList;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JOptionPane;

/**
 *
 * @author Jamie
 */
public class DatabaseConnector {

    private String dbName;
    private Connection conn;
    private PreparedStatement pstmt;
    private ResultSet rset;

    public DatabaseConnector() {
        dbName = "PropertyRecords";
    }

    public void createConnection() throws SQLException, ClassNotFoundException {
        String userId = "prdcd";
        String password = "4Bigtittycowgirl!";

        try {
            Class.forName("oracle.jdbc.driver.OracleDriver").newInstance();
            conn = DriverManager.getConnection("jdbc:oracle:thin:@tom.uopnet.plymouth.ac.uk:1521:orcl",
                    userId, password);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void closeConnection() throws SQLException, ClassNotFoundException {
        conn.close();
    }

    public PropertyList allProperties() throws SQLException, ClassNotFoundException {
        String queryString;
        PropertyList properties;

        queryString = "SELECT * FROM property";
        pstmt = conn.prepareStatement(queryString);
        rset = pstmt.executeQuery();

        properties = new PropertyList();
        while (rset.next()) {
            properties.addProperty(new Property(Integer.parseInt(rset.getString(1)), rset.getString(2), rset.getString(3), rset.getString(4), Integer.parseInt(rset.getString(5)), Integer.parseInt(rset.getString(6)), rset.getString(7), rset.getString(8), Integer.parseInt(rset.getString(9)), rset.getString(10), rset.getString(11), rset.getString(12), rset.getString(13), rset.getString(14), rset.getString(15)));
        }

        return properties;
    }

    public void addProperty(Property propertyToAdd) throws SQLException, ClassNotFoundException {
        String queryString;

        String propertyID = String.valueOf(propertyToAdd.getPropertyID());
        String houseNoName = String.valueOf(propertyToAdd.getHouseNoName());
        String postcode = propertyToAdd.getPostcode();
        String landlordID = String.valueOf(propertyToAdd.getLandlordID());
        String noOfBeds = String.valueOf(propertyToAdd.getNoOfBeds());
        String propertyType = propertyToAdd.getPropertyType();
        String propertyDescription = propertyToAdd.getPropertyDescription();
        String marketTypeID = String.valueOf(propertyToAdd.getMarketTypeID());
        String addressLine1 = propertyToAdd.getAddressLine1();
        String addressLine2 = propertyToAdd.getAddressLine2();
        String county = propertyToAdd.getCounty();
        String town_City = propertyToAdd.getTown_City();
        String imageName = propertyToAdd.getImageName();

        queryString = "INSERT INTO property (propertyid, housenumber_name, postcode, landlordid, numberofbed, propertytype, propertydescription, markettypeid, addressline1, addressline2, county, town_city, imagename, available)"
                + " VALUES ('" + propertyID + "', '" + houseNoName + "', '" + postcode
                + "', '" + landlordID + "', '" + noOfBeds + "', '" + propertyType + "', '" + propertyDescription
                + "', '" + marketTypeID + "', '" + addressLine1 + "', '" + addressLine2 + "', '" + county
                + "', '" + town_City + "', '" + imageName + "', 'T')";
        pstmt = conn.prepareStatement(queryString);
        rset = pstmt.executeQuery();
    }

    public void addLandlord(Landlord landlordToAdd) throws SQLException, ClassNotFoundException {
        String queryString;

        String landlordID = String.valueOf(landlordToAdd.getLandlordID());
        String forename = String.valueOf(landlordToAdd.getForename());
        String surname = landlordToAdd.getSurname();
        String email = String.valueOf(landlordToAdd.getEmail());
        String phoneNumber = String.valueOf(landlordToAdd.getPhoneNo());
        String postcode = landlordToAdd.getPostcode();
        String addressLine1 = landlordToAdd.getAddressLine1();
        String addressLine2 = landlordToAdd.getAddressLine2();
        String county = landlordToAdd.getCounty();
        String town_City = landlordToAdd.getTown_City();

        queryString = "INSERT INTO landlord (landlordid, forename, surname, email, phonenumber, postcode, addressline1, addressline2, county, town_city)"
                + " VALUES ('" + landlordID + "', '" + forename + "', '" + surname
                + "', '" + email + "', '" + phoneNumber + "', '" + postcode + "', '" + addressLine1
                + "', '" + addressLine2 + "', '" + county + "', '" + town_City + "')";
        pstmt = conn.prepareStatement(queryString);
        rset = pstmt.executeQuery();
    }

    public void deleteProperty(Property propertyToDelete) throws SQLException, ClassNotFoundException {
        String queryString;

        String propertyID = String.valueOf(propertyToDelete.getPropertyID());

        queryString = "DELETE FROM property "
                + "WHERE propertyid = '" + propertyID + "'";
        pstmt = conn.prepareStatement(queryString);
        rset = pstmt.executeQuery();

    }

    public void deleteLandlord(Landlord landlordToDelete) throws SQLException, ClassNotFoundException {
        String queryString;

        String landlordID = String.valueOf(landlordToDelete.getLandlordID());

        queryString = "DELETE FROM landlord "
                + "WHERE landlordid = '" + landlordID + "'";
        pstmt = conn.prepareStatement(queryString);
        rset = pstmt.executeQuery();

    }

    public void deleteTenant(Tenant tenantToDelete) throws SQLException, ClassNotFoundException {
        String queryString;

        String tenantID = String.valueOf(tenantToDelete.getTenantID());

        queryString = "DELETE FROM tenant "
                + "WHERE tenantid = '" + tenantID + "'";
        pstmt = conn.prepareStatement(queryString);
        rset = pstmt.executeQuery();

    }

    public void CheckRepair(Repair repairToCheck, String approveDecline) throws SQLException, ClassNotFoundException {

        String queryString;

        //Increasing the repairhistoryID

        queryString = "SELECT repairhistoryid FROM repairhistory "
                + "ORDER BY repairhistoryid DESC";
        pstmt = conn.prepareStatement(queryString);
        rset = pstmt.executeQuery();

        rset.next();

        String repairHistoryID = String.valueOf(Integer.parseInt(rset.getString(1)) + 1);

        //Setting the date of the change - today's date

        Date date = new Date();

        SimpleDateFormat ft = new SimpleDateFormat("dd-MMM-yy");

        String theDate = ft.format(date);

        //Setting the changeDescription

        String changeDescription;

        if ("decline".equals(approveDecline)) {

            changeDescription = "Problem declined";
        } else {

            changeDescription = "Problem approved";
        }

        //Setting the propertyID

        String propertyID = String.valueOf(repairToCheck.getPropertyID());

        //Adding the new record into repairhistory

        queryString = "INSERT INTO repairhistory (REPAIRHISTORYID, DATEOFCHANGE, CHANGEDESCRIPTION, PROPERTYID) "
                + "VALUES ('" + repairHistoryID + "', '" + theDate + "', '" + changeDescription + "', '" + propertyID + "')";

        pstmt = conn.prepareStatement(queryString);
        rset = pstmt.executeQuery();

        //Approving the repair in the system

        String repairID = String.valueOf(repairToCheck.getRepairID());

        queryString = "UPDATE repair "
                + "SET checked = 'Y' "
                + "WHERE repairid = '" + repairID + "'";
        pstmt = conn.prepareStatement(queryString);
        rset = pstmt.executeQuery();
    }

    public void editProperty(Property propertyToEdit) throws SQLException, ClassNotFoundException {

        String queryString;

        String propertyID = String.valueOf(propertyToEdit.getPropertyID());
        String houseNoName = String.valueOf(propertyToEdit.getHouseNoName());
        String postcode = propertyToEdit.getPostcode();
        String noOfBeds = String.valueOf(propertyToEdit.getNoOfBeds());
        String propertyType = propertyToEdit.getPropertyType();
        String propertyDescription = propertyToEdit.getPropertyDescription();
        String marketTypeID = String.valueOf(propertyToEdit.getMarketTypeID());
        String addressLine1 = propertyToEdit.getAddressLine1();
        String addressLine2 = propertyToEdit.getAddressLine2();
        String county = propertyToEdit.getCounty();
        String town_City = propertyToEdit.getTown_City();
        String tenantID = propertyToEdit.getTenantID();

        queryString = "UPDATE property"
                + " SET tenantid = '" + tenantID + "', housenumber_name = '" + houseNoName + "', postcode = '" + postcode
                + "', numberofbed = '" + noOfBeds + "', propertyType = '" + propertyType + "', propertydescription = '" + propertyDescription
                + "', markettypeid = '" + marketTypeID + "', addressline1 = '" + addressLine1 + "', addressline2 = '" + addressLine2 + "', county = '" + county
                + "', town_city = '" + town_City + "'"
                + "WHERE propertyid = '" + propertyID + "'";
        pstmt = conn.prepareStatement(queryString);
        rset = pstmt.executeQuery();
    }

    public void editLandlord(Landlord landlordToEdit) throws SQLException, ClassNotFoundException {

        String queryString;

        String landlordID = String.valueOf(landlordToEdit.getLandlordID());
        String forename = String.valueOf(landlordToEdit.getForename());
        String surname = landlordToEdit.getSurname();
        String email = String.valueOf(landlordToEdit.getEmail());
        String phoneNumber = String.valueOf(landlordToEdit.getPhoneNo());
        String postcode = landlordToEdit.getPostcode();
        String addressLine1 = landlordToEdit.getAddressLine1();
        String addressLine2 = landlordToEdit.getAddressLine2();
        String county = landlordToEdit.getCounty();
        String town_City = landlordToEdit.getTown_City();

        queryString = "UPDATE landlord"
                + " SET forename = '" + forename + "', surname = '" + surname
                + "', email = '" + email + "', phonenumber = '" + phoneNumber + "', postcode = '" + postcode
                + "', addressline1 = '" + addressLine1 + "', addressline2 = '" + addressLine2 + "', county = '" + county
                + "', town_city = '" + town_City + "'"
                + " WHERE landlordid = '" + landlordID + "'";
        pstmt = conn.prepareStatement(queryString);
        rset = pstmt.executeQuery();
    }

    public void editTenant(Tenant tenantToEdit) throws SQLException, ClassNotFoundException {

        String queryString;

        String tenantID = String.valueOf(tenantToEdit.getTenantID());
        String forename = String.valueOf(tenantToEdit.getForename());
        String surname = tenantToEdit.getSurname();
        String email = String.valueOf(tenantToEdit.getEmail());
        String phoneNumber = tenantToEdit.getPhoneNo();

        queryString = "UPDATE tenant "
                + " SET forename = '" + forename + "', surname = '" + surname
                + "', email = '" + email + "', phonenumber = '" + phoneNumber + "'"
                + " WHERE tenantid = '" + tenantID + "'";
        pstmt = conn.prepareStatement(queryString);
        rset = pstmt.executeQuery();
    }

    public void deleteEnquiry(Enquiry enquiryToDelete) throws SQLException, ClassNotFoundException {

        String queryString, enquiryID = String.valueOf(enquiryToDelete.getID());

        //Increasing the repairhistoryID

        queryString = "UPDATE enquiry "
                + "SET landlord_notified = 'Y' "
                + "WHERE enquiryid = '" + enquiryID + "'";
        pstmt = conn.prepareStatement(queryString);
        rset = pstmt.executeQuery();

    }

    public LandlordList allLandlords() throws SQLException, ClassNotFoundException {
        String queryString;
        LandlordList landlords;

        queryString = "SELECT * FROM landlord";
        pstmt = conn.prepareStatement(queryString);
        rset = pstmt.executeQuery();

        landlords = new LandlordList();
        while (rset.next()) {
            landlords.addLandlord(new Landlord(Integer.parseInt(rset.getString(1)), rset.getString(2), rset.getString(3), rset.getString(4), rset.getString(5), rset.getString(6), rset.getString(7), rset.getString(8), rset.getString(9), rset.getString(10)));
        }

        return landlords;
    }

    public TenantList allTenants() throws SQLException, ClassNotFoundException {
        String queryString;
        TenantList tenants;

        queryString = "SELECT * FROM tenant";
        pstmt = conn.prepareStatement(queryString);
        rset = pstmt.executeQuery();

        tenants = new TenantList();
        while (rset.next()) {
            tenants.addTenant(new Tenant(rset.getString(1), rset.getString(2), rset.getString(3), rset.getString(4), rset.getString(5), Integer.parseInt(rset.getString(6))));
        }

        return tenants;
    }

    public RepairList allRepairs() throws SQLException, ClassNotFoundException {
        String queryString;
        RepairList repairs;

        queryString = "SELECT * FROM repair "
                + "WHERE checked = 'N'";
        pstmt = conn.prepareStatement(queryString);
        rset = pstmt.executeQuery();

        repairs = new RepairList();
        while (rset.next()) {
            repairs.addRepair(new Repair(Integer.parseInt(rset.getString(1)), rset.getString(2), Integer.parseInt(rset.getString(3))));
        }

        return repairs;
    }

    public EnquiryList allEnquiries() throws SQLException, ClassNotFoundException {
        String queryString;
        EnquiryList enquiries;

        queryString = "SELECT enquiry.enquiryid, guest.email, guest.phonenumber, enquiry.propertyid "
                + "FROM enquiry "
                + "INNER JOIN guest "
                + "ON enquiry.guestid=guest.guestid "
                + "WHERE landlord_notified = 'N'";
        pstmt = conn.prepareStatement(queryString);
        rset = pstmt.executeQuery();

        enquiries = new EnquiryList();
        while (rset.next()) {
            enquiries.addEnquiry(new Enquiry(Integer.parseInt(rset.getString(1)), rset.getString(2), rset.getString(3), Integer.parseInt(rset.getString(4))));
        }

        return enquiries;
    }

    public String fetchLandlord(String landlordID) throws SQLException, ClassNotFoundException {
        String queryString, name;

        queryString = "SELECT forename || ' ' || surname AS full_name "
                + "FROM landlord "
                + "WHERE landlordid = '" + landlordID + "'";
        pstmt = conn.prepareStatement(queryString);
        rset = pstmt.executeQuery();

        rset.next();

        name = rset.getString(1);

        return name;

    }

    public String[] fetchProperty(String tenantID) throws SQLException, ClassNotFoundException {

        String[] house = new String[11];
        String queryString;

        queryString = "SELECT housenumber_name, property.addressline1, property.addressline2, property.county, property.town_city, property.postcode, landlord.forename || ' ' || landlord.surname AS landlordname, numberofbed, propertytype, markettype.type, propertydescription "
                + "FROM property "
                + "INNER JOIN markettype "
                + "ON property.markettypeid = markettype.markettypeid "
                + "INNER JOIN landlord "
                + "ON landlord.landlordid = property.landlordid "
                + "WHERE tenantid = '" + tenantID + "'";
        pstmt = conn.prepareStatement(queryString);
        rset = pstmt.executeQuery();
        rset.next();

        for (int i = 0; i < 11; i++) {
            house[i] = rset.getString(i + 1);
        }

        if (house[2] == null) {
            house[2] = "N/A";
        }

        return house;
    }

    public ArrayList<String> housesOwned(String landlordID) throws SQLException, ClassNotFoundException {

        ArrayList<String> housesOwned = new ArrayList<>();
        String queryString;

        queryString = "SELECT housenumber_name || ' ' || addressline1 || ', ' || town_city || ', ' || county AS full_line "
                + "FROM property "
                + "WHERE landlordid = '" + landlordID + "'";
        pstmt = conn.prepareStatement(queryString);
        rset = pstmt.executeQuery();

        while (rset.next()) {
            housesOwned.add(rset.getString(1));
        }

        if (housesOwned.size() < 1) {
            housesOwned.add("No houses owned.");
        }

        return housesOwned;
    }

    public String[] fetchRepair(String repairID) throws SQLException, ClassNotFoundException {

        String[] repairs = new String[6];
        String queryString;

        queryString = "SELECT repair.repairid, repair.propertyid, property.housenumber_name, property.addressline1, property.postcode, repair.description "
                + "FROM repair "
                + "INNER JOIN property "
                + "ON repair.propertyid = property.propertyid "
                + "WHERE repair.repairid = '" + repairID + "'";
        pstmt = conn.prepareStatement(queryString);
        rset = pstmt.executeQuery();
        rset.next();

        for (int i = 0; i < 6; i++) {
            repairs[i] = rset.getString(i + 1);
        }

        return repairs;
    }

    public String[] fetchEnquiry(String enquiryID) throws SQLException, ClassNotFoundException {

        String[] enquiry = new String[7];
        String queryString;

        queryString = "SELECT enquiry.enquiryid, enquiry.propertyid, property.addressline1, property.addressline2, property.postcode, guest.email, guest.phonenumber "
                + "FROM enquiry "
                + "INNER JOIN property "
                + "ON property.propertyid = enquiry.propertyid "
                + "INNER JOIN guest "
                + "ON guest.guestid = enquiry.guestid "
                + "WHERE enquiry.enquiryid = '" + enquiryID + "'";

        pstmt = conn.prepareStatement(queryString);
        rset = pstmt.executeQuery();
        rset.next();

        for (int i = 0; i < 7; i++) {
            enquiry[i] = rset.getString(i + 1);
        }

        return enquiry;
    }

    public boolean testLogin(String emailAddress, String password) throws SQLException, ClassNotFoundException {

        boolean successful = false;
        String actualPassword = "";

        String queryString;

        queryString = "SELECT password "
                + "FROM admin "
                + "WHERE email = '" + emailAddress + "'";
        pstmt = conn.prepareStatement(queryString);
        rset = pstmt.executeQuery();

        if (rset.next()) {

            actualPassword = rset.getString(1);

            if (password.equals(actualPassword)) {

                successful = true;
            } else {
                JOptionPane.showMessageDialog(null, "Wrong username/password!");
                successful = false;
            }
        } else {

            JOptionPane.showMessageDialog(null, "Wrong username/password!");
            successful = false;
        }

        return successful;

    }
}
